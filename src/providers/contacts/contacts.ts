import { Injectable } from '@angular/core';
import { Contacts } from '@ionic-native/contacts';


/*
  Generated class for the ContactsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ContactsProvider {

  contactList:any;

  constructor(public contacts: Contacts) {
    this.contactList = [];
  }

  /*getContacts() {

    let urlSearchParams = new URLSearchParams();
    urlSearchParams.append('results',"50");

    let requestOptions = new RequestOptions({'params': urlSearchParams});

    return this.http.get('https://randomuser.me/api/', requestOptions)
    .map(response => response.json());
  }*/

  getContacts(){
    
    return new Promise((resolve, reject) => { 
      this.contacts.find(
      ["displayName", "phoneNumbers","emails"],
      {multiple: true, hasPhoneNumber: true}
      )
      .then((contacts) => { 
        console.log(contacts);

        for (var i=0 ; i < contacts.length; i++){
          if(contacts[i].displayName !== null) {
            var contact = {};
            contact["name"]   = contacts[i].displayName;

            if( contacts[i].phoneNumbers !== null ){
              contact["number"] = contacts[i].phoneNumbers[0].value;
            }else{
              contact["number"] = '34 99999-9999'
            }

            if( contacts[i].emails !== null){
              contact["email"] = contacts[i].emails[0].value;
            }else{
              contact["email"] = 'nada'
            }
            this.contactList.push(contact);
          }
        }
        resolve(this.contactList.sort((a,b)=>{
          if (a.name > b.name) {
            return 1;
          }
          if (a.name < b.name) {
            return -1;
          }
          // a must be equal to b
          return 0;
        }));
      })
      .catch((err)=>{
        reject(err);
      });
    })

  }

}
