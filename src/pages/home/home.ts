import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ContactsProvider } from '../../providers/contacts/contacts';
import { ContactPage } from '../contact/contact';
import { ToastController, LoadingController } from 'ionic-angular'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  contacts: any;
  allcontacts: any;

  constructor(private loadingController: LoadingController,
    private toastController: ToastController,
    public contactsProvider: ContactsProvider,
    public navCtrl: NavController) {

  }

  onContactClick(contact) {
    this.navCtrl.push(ContactPage,{'contact':contact});
  }

  ionViewDidLoad() {
    const loading = this.loadingController.create({
      content: 'Carregando Contatos, Aguarde...'
    });
    loading.present();

    this.contactsProvider.getContacts()
    .then((result)=>{
      this.contacts   = result;
      this.allcontacts = result;
    });

    loading.dismiss();

  }

  filterContacts(event) {
    let searchvalue = event.target.value;
    if(searchvalue && searchvalue !== "") {
      searchvalue = searchvalue.toLowerCase();
      this.contacts = this.allcontacts.filter((contact) => {
          let contactname = (`${contact.name}`).toLowerCase();
          return (contactname.indexOf(searchvalue) > -1);
      });
    } else {
      this.contacts = this.allcontacts;
    }
  }

  private showToast(message: string) {
    let toast = this.toastController.create({
      'message': message,
      'duration': 2000
    });
    toast.present();
  }

}
